<?
/**
 * Media Service, LLC
 *
 * @author Yurii Kozlov <y.kozlov08@gmail.com>
 */
require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin.php");

$modulePermissions = $APPLICATION->GetGroupRight("medias.main");

if ($modulePermissions == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require_once ($_SERVER["DOCUMENT_ROOT"] . "/local/modules/medias.main/include.php");

IncludeModuleLangFile(__FILE__);

ClearVars();
$errorMessage = "";

\Bitrix\Main\Page\Asset::getInstance()->addJs($_SERVER["DOCUMENT_ROOT"] . "/local/modules/medias.main/js/script.js");
/*
 * Тут пишем оброботчики данніх из нашей формы, которую определим ниже.
 */

$APPLICATION->SetTitle('Дані віддаленого сервера');

$aTabs = array(
    array(
        "DIV" => "tab1",
        "TAB" => GetMessage("TAB_1"),
        // "ICON" => "medias.main",
        "TITLE" => GetMessage("TAB_1")
    )
);
 
$tabControl = new \CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>
<?
/*
 * Тут пишем то, что будет общим для всех вкладок на странице. Например шапку формі.
 */
?>
<?
$tabControl->BeginNextTab();
?>
<div class="medias_container" style="display:flex; flex-direction:column;">
	<div class="buttons_set">
    	<a id="downloadButton" href="#" style="display:none;" class="ui-btn ui-btn-primary-dark ui-btn-icon-download ">Скачати .xlsx</a>
    </div>
	<div class="medias_main_tab_content">
    
            <?

            $APPLICATION->IncludeComponent('medias:main.ui.filter', 'medias_admin_task_report', [
                'FILTER_ID' => 'task_report',
                'GRID_ID' => 'task_report',
                'FILTER' => [
                    [
                        'id' => 'DATE',
                        'name' => 'Дата',
                        'type' => 'date'
                    ]
                ],
                'ENABLE_LIVE_SEARCH' => false,
                'ENABLE_LABEL' => true
            ]);
            ?>
    
    </div>
    
	<div class="table_information">
		<template>
            <div>
                <data-table v-bind="bindings"/>
            </div>
        </template>
	</div>
</div>
<?
/*
 * Тут пишем то, что будет отображаться в первой вкладке.
 */
?>
<script type="text/javascript">

BX.addCustomEvent('BX.Main.Filter:apply', BX.delegate(function (filterId, dumpObject, filterObj) { 
	
    var workarea = $('#' + filterId); // в command будет храниться GRID_ID из фильтра 

	// send request to get table information
	
	let filterValues = filterObj.getFilterFieldsValues();

	if(filterValues.DATE_datesel == 'EXACT'){}

	let restParams = {};
    
	restParams.from = filterValues.DATE_from + ' 00:00';
	restParams.to = filterValues.DATE_to + ' 23:59';

	let restUrl = '<?=\Medias\Main\Integration\Podorognik::getRestUrl();?>';
	let restCode = '<?=\Medias\Main\Integration\Podorognik::getRestCode();?>';

	let url = restUrl + 'rest/' + restCode + 'medias.main.task.reports.podorognik.get.json';
	
	BX.ajax.post(
			url,
			restParams,
         function (data){

				let objectData = JSON.parse(data);
				
				let downloadButton = BX('downloadButton');

				downloadButton.href = objectData.result.downLoadLink;
				
				downloadButton.style.display = 'block';

				console.log(JSON.parse(data));
             }
        );
}));
</script>

<?
$tabControl->EndTab();

$tabControl->End();

require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>