<?php
defined('B_PROLOG_INCLUDED') || die();
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
/*
 * Media Sevice, LLC
 *
 * @author Yurii Kozlov <y.kozlov08@gmail.com>
 * @version 5.01.2022
 */
IncludeModuleLangFile(__FILE__);

class medias_interbitrix extends CModule
{

    var $MODULE_ID = "medias.interbitrix";    

    var $PARTNER_NAME = "Media Sevice";

    var $PARTNER_URI = "http://medias.com.ua";

    /*
     * main function of class
     * check version
     */
    function medias_interbitrix()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include ($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = GetMessage("MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("MODULE_DESCRIPTION");
    }

    /*
     * install module files
     */
    function InstallFiles()
    {
        $application = \Bitrix\Main\Application::getInstance();

        $documentRoot = $application->getDocumentRoot();

        CopyDirFiles(__DIR__ . "/admin", $documentRoot . "/bitrix/admin/", true, true);

        if (is_dir(__DIR__ . "/files/components/medias")) {

            if (! is_dir($documentRoot . "/local"))
                mkdir($documentRoot . "/local");

            if (! is_dir($documentRoot . "/local/components"))
                mkdir($documentRoot . "/local/components");

            if (! is_dir($documentRoot . "/local/components/medias"))
                mkdir($documentRoot . "/local/components/medias");

            CopyDirFiles(__DIR__ . "/files/components/medias", $documentRoot . "/local/components/medias/", true, true);
        }

        return true;
    }

    /*
     * uninstall files
     */
    function UnInstallFiles()
    {
        return true;
    }

    /*
     * install module
     */
    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        // $this->InstallFiles();
        $this->InstallEvents();
        $this->InstallHl();
        $this->InstallUserFields();

        RegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile("Встановлення модуля " . $this->MODULE_NAME, $DOCUMENT_ROOT . "/local/modules/".$this->MODULE_ID."/install/step.php");
    }

    /*
     * unistall module
     */
    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->UnInstallFiles();
        $this->UnInstallEvents();

        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile("Деінсталляція модуля " . $this->MODULE_NAME, $DOCUMENT_ROOT . "/local/modules/".$this->MODULE_ID."/install/unstep.php");
    }

    function InstallEvents()
    {
        $eventManager = EventManager::getInstance();

        $eventManager->registerEventHandlerCompatible('tasks', 'OnTaskAdd', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'OnTaskAdd');
        $eventManager->registerEventHandlerCompatible('tasks', 'OnTaskUpdate', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'OnTaskUpdate');
        $eventManager->registerEventHandlerCompatible('tasks', 'OnBeforeTaskUpdate', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'OnBeforeTaskUpdate');

        $eventManager->registerEventHandlerCompatible('forum', 'OnAfterCommentAdd', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'OnTaskCommentAdd');

        $eventManager->registerEventHandlerCompatible('rest', 'OnRestServiceBuildDescription', $this->MODULE_ID, 'Medias\Main\Integration\Api', 'registerInterface');

        $eventManager->registerEventHandlerCompatible('main', 'OnPageStart', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'start');
    }

    function UnInstallEvents()
    {
        $eventManager = EventManager::getInstance();

        $eventManager->unRegisterEventHandler('tasks', 'OnTaskAdd', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'OnTaskAdd');
        $eventManager->unRegisterEventHandler('tasks', 'OnTaskUpdate', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'OnTaskUpdate');
        $eventManager->unRegisterEventHandler('tasks', 'OnBeforeTaskUpdate', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'OnBeforeTaskUpdate');

        $eventManager->unRegisterEventHandler('forum', 'OnAfterCommentAdd', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'OnAfterCommentAdd');

        $eventManager->unRegisterEventHandler('rest', 'OnRestServiceBuildDescription', $this->MODULE_ID, 'Medias\Main\Integration\Api', 'registerInterface');

        $eventManager->unRegisterEventHandler('main', 'OnPageStart', $this->MODULE_ID, 'Medias\Main\Integration\Podorognik', 'start');
    }

    function InstallHl()
    {
        $this->userListHl();
        $this->taskRegisterHl();
        $this->taskCommentRegisterHl();
        $this->fileRegister();
    }

    function InstallUserFields()
    {
        $this->createTaskUserFields();
    }

    function userListHl()
    {
        Loader::IncludeModule('highloadblock');

        $arLangs = [];

        $arLangs = [
            'ru' => 'Довідник користувачів',
            'ua' => 'Довідник користувачів',
            'en' => 'Users List'
        ];

        $dbRes = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
            'filter' => array(
                'TABLE_NAME' => 'm_remote_user'
            )
        ));

        if ($hlData = $dbRes->fetch()) {

            $id = $hlData['ID'];

            $UFObject = 'HLBLOCK_' . $id;

            $arCartFields = [
                'UF_USER_ID' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_USER_ID',
                    'USER_TYPE_ID' => 'integer',
                    'MANDATORY' => 'Y',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Ідентифікатор локального користувача',
                        'ua' => 'Ідентифікатор локального користувача',
                        'en' => 'Local user Id'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Ідентифікатор локального користувача',
                        'ua' => 'Ідентифікатор локального користувача',
                        'en' => 'Local user Id'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Ідентифікатор локального користувача',
                        'ua' => 'Ідентифікатор локального користувача',
                        'en' => 'Local user Id'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ],
                'UF_REMOTE_USER_ID' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_REMOTE_USER_ID',
                    'USER_TYPE_ID' => 'integer',
                    'MANDATORY' => 'Y',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Ідентифікатор віддаленого користувача',
                        'ua' => 'Ідентифікатор віддаленого користувача',
                        'en' => 'Remote user Id'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Ідентифікатор віддаленого користувача',
                        'ua' => 'Ідентифікатор віддаленого користувача',
                        'en' => 'Remote user Id'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Ідентифікатор віддаленого користувача',
                        'ua' => 'Ідентифікатор віддаленого користувача',
                        'en' => 'Remote user Id'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ],
                'UF_EMAIL' => Array(
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_EMAIL',
                    'USER_TYPE_ID' => 'string',
                    'MANDATORY' => 'N',
                    "EDIT_FORM_LABEL" => Array(
                        'ru' => 'Почта локального користувача',
                        'ua' => 'Почта локального користувача',
                        'en' => 'Local user email'
                    ),
                    "LIST_COLUMN_LABEL" => Array(
                        'ru' => 'Почта локального користувача',
                        'ua' => 'Почта локального користувача',
                        'en' => 'Local user email'
                    ),
                    "LIST_FILTER_LABEL" => Array(
                        'ru' => 'Почта локального користувача',
                        'ua' => 'Почта локального користувача',
                        'en' => 'Local user email'
                    ),
                    "ERROR_MESSAGE" => Array(
                        'ru' => '',
                        'en' => ''
                    ),
                    "HELP_MESSAGE" => Array(
                        'ru' => '',
                        'en' => ''
                    )
                )
            ];

            $arSavedFieldsRes = Array();

            foreach ($arCartFields as $arCartField) {

                $obUserField = new \CUserTypeEntity();

                $ID = $obUserField->Add($arCartField);

                $arSavedFieldsRes[] = $ID;
            }
        } else {

            $result = HL\HighloadBlockTable::add(array(
                'NAME' => 'RemoteUser',
                'TABLE_NAME' => 'm_remote_user'
            ));

            if ($result->isSuccess()) {

                $id = $result->getId();

                foreach ($arLangs as $lang_key => $lang_val) {
                    HL\HighloadBlockLangTable::add(array(
                        'ID' => $id,
                        'LID' => $lang_key,
                        'NAME' => $lang_val
                    ));
                }

                $UFObject = 'HLBLOCK_' . $id;

                $arCartFields = [
                    'UF_USER_ID' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_USER_ID',
                        'USER_TYPE_ID' => 'integer',
                        'MANDATORY' => 'Y',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Ідентифікатор локального користувача',
                            'ua' => 'Ідентифікатор локального користувача',
                            'en' => 'Local user Id'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Ідентифікатор локального користувача',
                            'ua' => 'Ідентифікатор локального користувача',
                            'en' => 'Local user Id'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Ідентифікатор локального користувача',
                            'ua' => 'Ідентифікатор локального користувача',
                            'en' => 'Local user Id'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ],
                    'UF_REMOTE_USER_ID' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_REMOTE_USER_ID',
                        'USER_TYPE_ID' => 'integer',
                        'MANDATORY' => 'Y',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Ідентифікатор віддаленого користувача',
                            'ua' => 'Ідентифікатор віддаленого користувача',
                            'en' => 'Remote user Id'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Ідентифікатор віддаленого користувача',
                            'ua' => 'Ідентифікатор віддаленого користувача',
                            'en' => 'Remote user Id'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Ідентифікатор віддаленого користувача',
                            'ua' => 'Ідентифікатор віддаленого користувача',
                            'en' => 'Remote user Id'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ],
                    'UF_EMAIL' => Array(
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_EMAIL',
                        'USER_TYPE_ID' => 'string',
                        'MANDATORY' => 'N',
                        "EDIT_FORM_LABEL" => Array(
                            'ru' => 'Почта локального користувача',
                            'ua' => 'Почта локального користувача',
                            'en' => 'Local user email'
                        ),
                        "LIST_COLUMN_LABEL" => Array(
                            'ru' => 'Почта локального користувача',
                            'ua' => 'Почта локального користувача',
                            'en' => 'Local user email'
                        ),
                        "LIST_FILTER_LABEL" => Array(
                            'ru' => 'Почта локального користувача',
                            'ua' => 'Почта локального користувача',
                            'en' => 'Local user email'
                        ),
                        "ERROR_MESSAGE" => Array(
                            'ru' => '',
                            'en' => ''
                        ),
                        "HELP_MESSAGE" => Array(
                            'ru' => '',
                            'en' => ''
                        )
                    )
                ];

                $arSavedFieldsRes = Array();

                foreach ($arCartFields as $arCartField) {

                    $obUserField = new \CUserTypeEntity();

                    $ID = $obUserField->Add($arCartField);

                    $arSavedFieldsRes[] = $ID;
                }
            }
        }
    }

    function taskRegisterHl()
    {
        Loader::IncludeModule('highloadblock');

        $arLangs = [];

        $arLangs = [
            'ru' => 'Довідник задач',
            'ua' => 'Довідник задач',
            'en' => 'Task List'
        ];

        $dbRes = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
            'filter' => array(
                'TABLE_NAME' => 'm_task_medias_register'
            )
        ));

        if ($hlData = $dbRes->fetch()) {

            $id = $hlData['ID'];

            $UFObject = 'HLBLOCK_' . $id;

            $arCartFields = [
                'UF_LOCAL_TASK_ID' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_LOCAL_TASK_ID',
                    'USER_TYPE_ID' => 'integer',
                    'MANDATORY' => 'Y',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Ідентифікатор локальної задачі',
                        'ua' => 'Ідентифікатор локальної задачі',
                        'en' => 'Local task Id'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Ідентифікатор локальної задачі',
                        'ua' => 'Ідентифікатор локальної задачі',
                        'en' => 'Local task Id'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Ідентифікатор локальної задачі',
                        'ua' => 'Ідентифікатор локальної задачі',
                        'en' => 'Local task Id'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ],
                'UF_REMOTE_TASK_ID' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_REMOTE_TASK_ID',
                    'USER_TYPE_ID' => 'integer',
                    'MANDATORY' => 'Y',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Ідентифікатор віддаленої зідачі',
                        'ua' => 'Ідентифікатор віддаленої зідачі',
                        'en' => 'Remote task Id'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Ідентифікатор віддаленої зідачі',
                        'ua' => 'Ідентифікатор віддаленої зідачі',
                        'en' => 'Remote task Id'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Ідентифікатор віддаленої зідачі',
                        'ua' => 'Ідентифікатор віддаленої зідачі',
                        'en' => 'Remote task Id'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ],
                'UF_REMOTE_UPDATE' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_REMOTE_UPDATE',
                    'USER_TYPE_ID' => 'boolean',
                    'MANDATORY' => 'N',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Змінено віддалено',
                        'ua' => 'Змінено віддалено',
                        'en' => 'Змінено віддалено'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Змінено віддалено',
                        'ua' => 'Змінено віддалено',
                        'en' => 'Змінено віддалено'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Змінено віддалено',
                        'ua' => 'Змінено віддалено',
                        'en' => 'Змінено віддалено'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ]
            ];

            $arSavedFieldsRes = Array();

            foreach ($arCartFields as $arCartField) {

                $obUserField = new \CUserTypeEntity();

                $ID = $obUserField->Add($arCartField);

                $arSavedFieldsRes[] = $ID;
            }
        } else {

            $result = HL\HighloadBlockTable::add(array(
                'NAME' => 'TaskMediasRegister',
                'TABLE_NAME' => 'm_task_medias_register'
            ));

            if ($result->isSuccess()) {

                $id = $result->getId();

                foreach ($arLangs as $lang_key => $lang_val) {
                    HL\HighloadBlockLangTable::add(array(
                        'ID' => $id,
                        'LID' => $lang_key,
                        'NAME' => $lang_val
                    ));
                }

                $UFObject = 'HLBLOCK_' . $id;

                $arCartFields = [
                    'UF_LOCAL_TASK_ID' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_LOCAL_TASK_ID',
                        'USER_TYPE_ID' => 'integer',
                        'MANDATORY' => 'N',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Ідентифікатор локальної задачі',
                            'ua' => 'Ідентифікатор локальної задачі',
                            'en' => 'Local task Id'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Ідентифікатор локальної задачі',
                            'ua' => 'Ідентифікатор локальної задачі',
                            'en' => 'Local task Id'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Ідентифікатор локальної задачі',
                            'ua' => 'Ідентифікатор локальної задачі',
                            'en' => 'Local task Id'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ],
                    'UF_REMOTE_TASK_ID' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_REMOTE_TASK_ID',
                        'USER_TYPE_ID' => 'integer',
                        'MANDATORY' => 'N',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Ідентифікатор віддаленої зідачі',
                            'ua' => 'Ідентифікатор віддаленої зідачі',
                            'en' => 'Remote task Id'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Ідентифікатор віддаленої зідачі',
                            'ua' => 'Ідентифікатор віддаленої зідачі',
                            'en' => 'Remote task Id'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Ідентифікатор віддаленої зідачі',
                            'ua' => 'Ідентифікатор віддаленої зідачі',
                            'en' => 'Remote task Id'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ],
                    'UF_REMOTE_UPDATE' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_REMOTE_UPDATE',
                        'USER_TYPE_ID' => 'boolean',
                        'MANDATORY' => 'N',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Змінено віддалено',
                            'ua' => 'Змінено віддалено',
                            'en' => 'Змінено віддалено'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Змінено віддалено',
                            'ua' => 'Змінено віддалено',
                            'en' => 'Змінено віддалено'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Змінено віддалено',
                            'ua' => 'Змінено віддалено',
                            'en' => 'Змінено віддалено'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ]
                ];

                $arSavedFieldsRes = Array();

                foreach ($arCartFields as $arCartField) {

                    $obUserField = new \CUserTypeEntity();

                    $ID = $obUserField->Add($arCartField);

                    $arSavedFieldsRes[] = $ID;
                }
            }
        }
    }

    function taskCommentRegisterHl()
    {
        Loader::IncludeModule('highloadblock');

        $arLangs = [];

        $arLangs = [
            'ru' => 'Довідник коментарів задач',
            'ua' => 'Довідник коментарів задач',
            'en' => 'Task Comment List'
        ];

        $dbRes = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
            'filter' => array(
                'TABLE_NAME' => 'm_task_comment_register'
            )
        ));

        if ($hlData = $dbRes->fetch()) {

            $id = $hlData['ID'];

            $UFObject = 'HLBLOCK_' . $id;
            
            $arCartFields = [
                'UF_TASK_LOCAL_ID' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_TASK_LOCAL_ID',
                    'USER_TYPE_ID' => 'integer',
                    'MANDATORY' => 'Y',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Ідентифікатор локальної задачі',
                        'ua' => 'Ідентифікатор локальної задачі',
                        'en' => 'Local task Id'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Ідентифікатор локальної задачі',
                        'ua' => 'Ідентифікатор локальної задачі',
                        'en' => 'Local task Id'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Ідентифікатор локальної задачі',
                        'ua' => 'Ідентифікатор локальної задачі',
                        'en' => 'Local task Id'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ],
                'UF_COMMENT_ID' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_COMMENT_ID',
                    'USER_TYPE_ID' => 'integer',
                    'MANDATORY' => 'N',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Ідентифікатор коментаря локальний',
                        'ua' => 'Ідентифікатор коментаря локальний',
                        'en' => 'Local comment Id'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Ідентифікатор коментаря локальний',
                        'ua' => 'Ідентифікатор коментаря локальний',
                        'en' => 'Local comment Id'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Ідентифікатор коментаря локальний',
                        'ua' => 'Ідентифікатор коментаря локальний',
                        'en' => 'Local comment Id'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ],
                'UF_COMMENT_REMOTE_ID' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_COMMENT_REMOTE_ID',
                    'USER_TYPE_ID' => 'integer',
                    'MANDATORY' => 'N',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Ідентифікатор коментаря віддалений',
                        'ua' => 'Ідентифікатор коментаря віддалений',
                        'en' => 'Remote comment Id'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Ідентифікатор коментаря віддалений',
                        'ua' => 'Ідентифікатор коментаря віддалений',
                        'en' => 'Remote comment Id'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Ідентифікатор коментаря віддалений',
                        'ua' => 'Ідентифікатор коментаря віддалений',
                        'en' => 'Remote comment Id'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ]
            ];
            
            $arSavedFieldsRes = Array();
            
            foreach ($arCartFields as $arCartField) {
                
                $obUserField = new \CUserTypeEntity();
                
                $ID = $obUserField->Add($arCartField);
                
                $arSavedFieldsRes[] = $ID;
            }
        } else {

            $result = HL\HighloadBlockTable::add(array(
                'NAME' => 'TaskCommentRegister',
                'TABLE_NAME' => 'm_task_comment_register'
            ));

            if ($result->isSuccess()) {

                $id = $result->getId();

                foreach ($arLangs as $lang_key => $lang_val) {
                    HL\HighloadBlockLangTable::add(array(
                        'ID' => $id,
                        'LID' => $lang_key,
                        'NAME' => $lang_val
                    ));
                }

                $UFObject = 'HLBLOCK_' . $id;

                $arCartFields = [
                    'UF_TASK_LOCAL_ID' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_TASK_LOCAL_ID',
                        'USER_TYPE_ID' => 'integer',
                        'MANDATORY' => 'Y',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Ідентифікатор локальної задачі',
                            'ua' => 'Ідентифікатор локальної задачі',
                            'en' => 'Local task Id'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Ідентифікатор локальної задачі',
                            'ua' => 'Ідентифікатор локальної задачі',
                            'en' => 'Local task Id'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Ідентифікатор локальної задачі',
                            'ua' => 'Ідентифікатор локальної задачі',
                            'en' => 'Local task Id'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ],
                    'UF_COMMENT_ID' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_COMMENT_ID',
                        'USER_TYPE_ID' => 'integer',
                        'MANDATORY' => 'N',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Ідентифікатор коментаря локальний',
                            'ua' => 'Ідентифікатор коментаря локальний',
                            'en' => 'Local comment Id'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Ідентифікатор коментаря локальний',
                            'ua' => 'Ідентифікатор коментаря локальний',
                            'en' => 'Local comment Id'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Ідентифікатор коментаря локальний',
                            'ua' => 'Ідентифікатор коментаря локальний',
                            'en' => 'Local comment Id'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ],
                    'UF_COMMENT_REMOTE_ID' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_COMMENT_REMOTE_ID',
                        'USER_TYPE_ID' => 'integer',
                        'MANDATORY' => 'N',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Ідентифікатор коментаря віддалений',
                            'ua' => 'Ідентифікатор коментаря віддалений',
                            'en' => 'Remote comment Id'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Ідентифікатор коментаря віддалений',
                            'ua' => 'Ідентифікатор коментаря віддалений',
                            'en' => 'Remote comment Id'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Ідентифікатор коментаря віддалений',
                            'ua' => 'Ідентифікатор коментаря віддалений',
                            'en' => 'Remote comment Id'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ]
                ];

                $arSavedFieldsRes = Array();

                foreach ($arCartFields as $arCartField) {

                    $obUserField = new \CUserTypeEntity();

                    $ID = $obUserField->Add($arCartField);

                    $arSavedFieldsRes[] = $ID;
                }
            }
        }
    }
    
    function fileRegister(){
        
        Loader::IncludeModule('highloadblock');
        
        $arLangs = [];
        
        $arLangs = [
            'ru' => 'Довідник файлів задач',
            'ua' => 'Довідник файлів задач',
            'en' => 'Task File List'
        ];
        
        $dbRes = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
            'filter' => array(
                'TABLE_NAME' => 'm_task_file_register'
            )
        ));
        
        if ($hlData = $dbRes->fetch()) {
            
            $id = $hlData['ID'];
            
            $UFObject = 'HLBLOCK_' . $id;
            
            $arCartFields = [
                'UF_LOCAL_FILE_ID' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_LOCAL_FILE_ID',
                    'USER_TYPE_ID' => 'integer',
                    'MANDATORY' => 'Y',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Ідентифікатор локального файлу',
                        'ua' => 'Ідентифікатор локального файлу',
                        'en' => 'Local File Id'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Ідентифікатор локального файлу',
                        'ua' => 'Ідентифікатор локального файлу',
                        'en' => 'Local File Id'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Ідентифікатор локального файлу',
                        'ua' => 'Ідентифікатор локального файлу',
                        'en' => 'Local File Id'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ],
                'UF_REMOTE_FILE_ID' => [
                    'ENTITY_ID' => $UFObject,
                    'FIELD_NAME' => 'UF_REMOTE_FILE_ID',
                    'USER_TYPE_ID' => 'integer',
                    'MANDATORY' => 'Y',
                    "EDIT_FORM_LABEL" => [
                        'ru' => 'Ідентифікатор віддаленого файлу',
                        'ua' => 'Ідентифікатор віддаленого файлу',
                        'en' => 'Remote File Id'
                    ],
                    "LIST_COLUMN_LABEL" => [
                        'ru' => 'Ідентифікатор віддаленого файлу',
                        'ua' => 'Ідентифікатор віддаленого файлу',
                        'en' => 'Remote File Id'
                    ],
                    "LIST_FILTER_LABEL" => [
                        'ru' => 'Ідентифікатор віддаленого файлу',
                        'ua' => 'Ідентифікатор віддаленого файлу',
                        'en' => 'Remote File Id'
                    ],
                    "ERROR_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ],
                    "HELP_MESSAGE" => [
                        'ru' => '',
                        'ua' => '',
                        'en' => ''
                    ]
                ],
            ];
            
            $arSavedFieldsRes = Array();
            
            foreach ($arCartFields as $arCartField) {
                
                $obUserField = new \CUserTypeEntity();
                
                $ID = $obUserField->Add($arCartField);
                
                $arSavedFieldsRes[] = $ID;
            }
        }else{
            
            $result = HL\HighloadBlockTable::add(array(
                'NAME' => 'TaskFileRegister',
                'TABLE_NAME' => 'm_task_file_register'
            ));
            
            if ($result->isSuccess()) {
                
                $id = $result->getId();
                
                foreach ($arLangs as $lang_key => $lang_val) {
                    HL\HighloadBlockLangTable::add(array(
                        'ID' => $id,
                        'LID' => $lang_key,
                        'NAME' => $lang_val
                    ));
                }
                
                $UFObject = 'HLBLOCK_' . $id;
                
                $arCartFields = [
                    'UF_LOCAL_FILE_ID' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_LOCAL_FILE_ID',
                        'USER_TYPE_ID' => 'integer',
                        'MANDATORY' => 'Y',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Ідентифікатор локального файлу',
                            'ua' => 'Ідентифікатор локального файлу',
                            'en' => 'Local File Id'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Ідентифікатор локального файлу',
                            'ua' => 'Ідентифікатор локального файлу',
                            'en' => 'Local File Id'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Ідентифікатор локального файлу',
                            'ua' => 'Ідентифікатор локального файлу',
                            'en' => 'Local File Id'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ],
                    'UF_REMOTE_FILE_ID' => [
                        'ENTITY_ID' => $UFObject,
                        'FIELD_NAME' => 'UF_REMOTE_FILE_ID',
                        'USER_TYPE_ID' => 'integer',
                        'MANDATORY' => 'Y',
                        "EDIT_FORM_LABEL" => [
                            'ru' => 'Ідентифікатор віддаленого файлу',
                            'ua' => 'Ідентифікатор віддаленого файлу',
                            'en' => 'Remote File Id'
                        ],
                        "LIST_COLUMN_LABEL" => [
                            'ru' => 'Ідентифікатор віддаленого файлу',
                            'ua' => 'Ідентифікатор віддаленого файлу',
                            'en' => 'Remote File Id'
                        ],
                        "LIST_FILTER_LABEL" => [
                            'ru' => 'Ідентифікатор віддаленого файлу',
                            'ua' => 'Ідентифікатор віддаленого файлу',
                            'en' => 'Remote File Id'
                        ],
                        "ERROR_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ],
                        "HELP_MESSAGE" => [
                            'ru' => '',
                            'ua' => '',
                            'en' => ''
                        ]
                    ],
                ];
                
                $arSavedFieldsRes = Array();
                
                foreach ($arCartFields as $arCartField) {
                    
                    $obUserField = new \CUserTypeEntity();
                    
                    $ID = $obUserField->Add($arCartField);
                    
                    $arSavedFieldsRes[] = $ID;
                }
            }
        }
    }

    function createTaskUserFields()
    {
        $arCartFields = [
            'UF_MEDIAS_TASK_REMOTE_ID' => [
                'ENTITY_ID' => 'TASKS_TASK',
                'FIELD_NAME' => 'UF_MEDIAS_TASK_REMOTE_ID',
                'USER_TYPE_ID' => 'integer',
                'MANDATORY' => 'N',
                "EDIT_FORM_LABEL" => [
                    'ru' => 'Ідентифікатор віддаленої задачі',
                    'ua' => 'Ідентифікатор віддаленої задачі',
                    'en' => 'Remote task Id'
                ],
                "LIST_COLUMN_LABEL" => [
                    'ru' => 'Ідентифікатор віддаленої задачі',
                    'ua' => 'Ідентифікатор віддаленої задачі',
                    'en' => 'Remote task Id'
                ],
                "LIST_FILTER_LABEL" => [
                    'ru' => 'Ідентифікатор віддаленої задачі',
                    'ua' => 'Ідентифікатор віддаленої задачі',
                    'en' => 'Remote task Id'
                ],
                "ERROR_MESSAGE" => [
                    'ru' => '',
                    'ua' => '',
                    'en' => ''
                ],
                "HELP_MESSAGE" => [
                    'ru' => '',
                    'ua' => '',
                    'en' => ''
                ]
            ],
            'UF_REMOTE_UPD' => [
                'ENTITY_ID' => 'TASKS_TASK',
                'FIELD_NAME' => 'UF_REMOTE_UPD',
                'USER_TYPE_ID' => 'boolean',
                'MANDATORY' => 'N',
                "EDIT_FORM_LABEL" => [
                    'ru' => '',
                    'ua' => '',
                    'en' => ''
                ],
                "LIST_COLUMN_LABEL" => [
                    'ru' => '',
                    'ua' => '',
                    'en' => ''
                ],
                "LIST_FILTER_LABEL" => [
                    'ru' => '',
                    'ua' => '',
                    'en' => ''
                ],
                "ERROR_MESSAGE" => [
                    'ru' => '',
                    'ua' => '',
                    'en' => ''
                ],
                "HELP_MESSAGE" => [
                    'ru' => '',
                    'ua' => '',
                    'en' => ''
                ]
            ]
        ];

        $arSavedFieldsRes = Array();

        foreach ($arCartFields as $arCartField) {

            $obUserField = new \CUserTypeEntity();

            $ID = $obUserField->Add($arCartField);

            $arSavedFieldsRes[] = $ID;
        }
    }
}

?>