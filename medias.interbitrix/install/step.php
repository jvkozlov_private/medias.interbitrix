<?
/**
 * Media Service, LLC
 *
 * @author Yurii Kozlov <y.kozlov08@gmail.com>
 */
if(!check_bitrix_sessid()) return;?>
<?
	$note = '';

	$note .= GetMessage("MODULE_INSTALLED");

	$note .= '\n';

	$note .= 'Для подальшого використання модуля виконайте налаштування модуля в розділі "Налаштування модулів"';

	echo CAdminMessage::ShowNote($note);
?>