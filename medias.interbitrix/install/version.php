<?
/**
 * Media Service, LLC
 *
 * @author Yurii Kozlov <y.kozlov08@gmail.com>
 *
 */

$arModuleVersion = array(
	"VERSION" => "0.8.00",
	"VERSION_DATE" => "2022-01-05 00:00:00"
);
?>