<?php
namespace Medias\Main\Integration;

class Rest
{

    function __construct($arParams = [])
    {
        if (! \Bitrix\Main\Loader::includeModule('main')) {

            return false;
        }
        if (! \Bitrix\Main\Loader::includeModule('tasks')) {

            return false;
        }
    }

    public function callCurl($arParams)
    {
        if (! function_exists('curl_init')) {
            return [
                'error' => 'error_php_lib_curl',
                'error_information' => 'need install curl lib'
            ];
        }
        $url = $arParams['url'];

       $sPostFields = http_build_query($arParams['params']);
       
        try {
            $obCurl = curl_init();
            curl_setopt($obCurl, CURLOPT_URL, $url);
            curl_setopt($obCurl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($obCurl, CURLOPT_POSTREDIR, 10);
            curl_setopt($obCurl, CURLOPT_USERAGENT, 'Bitrix24 CRest PHP 1.36');
            if ($sPostFields) {
                curl_setopt($obCurl, CURLOPT_POST, true);
                curl_setopt($obCurl, CURLOPT_POSTFIELDS, $sPostFields);
            }
            curl_setopt($obCurl, CURLOPT_FOLLOWLOCATION, (isset($arParams['followlocation'])) ? $arParams['followlocation'] : 1);
            if (isset($arParams['ssl_ignore']) && $arParams['ssl_ignore'] === true) {
                curl_setopt($obCurl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($obCurl, CURLOPT_SSL_VERIFYHOST, false);
            }
            $out = curl_exec($obCurl);
            $info = curl_getinfo($obCurl);
            if (curl_errno($obCurl)) {
                $info['curl_error'] = curl_error($obCurl);
            }

            $result = $out;
         
            curl_close($obCurl);
            
            $arErrorInform = [];

            if (! empty($result['error'])) {
                if (! empty($arErrorInform[$result['error']])) {
                    $result['error_information'] = $arErrorInform[$result['error']];
                }
            }
            if (! empty($info['curl_error'])) {
                $result['error'] = 'curl_error';
                $result['error_information'] = $info['curl_error'];
            }

            return $result;
        } catch (Exception $e) {

            return [
                'error' => 'exception',
                'error_exception_code' => $e->getCode(),
                'error_information' => $e->getMessage()
            ];
        }
    }

    public static function setLog($arData, $type = '')
    {}
}
?>