<?
/**
 *  Media Sevice, LLC
 *
 *@author Yurii Kozlov <y.kozlov08@gmail.com>
 */
namespace Medias\Main\General;

class Handler
{

    function __construct()
    {
        if (! \Bitrix\Main\Loader::includeModule('main')) {

            return false;
        }
    }

    public static function pre($arr)
    {
        if (\Bitrix\Main\Config\Option::get("medias.main", "function_pre")) {

            echo '<pre>';

            print_r($arr);

            echo '</pre>';
        }
    }

    public static function write($arFields, $name = "ex.log")
    {
        if (\Bitrix\Main\Config\Option::get("medias.main", "function_write")) {

            $docRoot = \Bitrix\Main\Application::getInstance()->getDocumentRoot();

            $logDir = $docRoot . '/upload/log';

            if (! is_dir($logDir)) {

                mkdir($logDir, 0777, true);
            }

            file_put_contents($logDir . '/' . $name, ":\n" . var_export($arFields, true) . "\n", FILE_APPEND);
        }
    }

    public static function ArConsole($arr, $login = '')
    {
        if (\Bitrix\Main\Config\Option::get("medias.main", "function_ArConsole")) {
            
            global $USER;
            
            if ($USER->IsAdmin()) {
                if ($login && $USER->GetLogin() != $login)
                    return false;

                echo '<script>console.log(' . \CUtil::PhpToJSObject($arr) . ');</script>';
            }
        }
    }

    public function eventLog(&$arFields)
    {
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/upload/log/ex.log', ":\n" . var_export($arFields, true) . "\n", FILE_APPEND);

        return false;
    }
    
    public function syncLog($arFields, $name = "syncLog.log"){
        
        $docRoot = \Bitrix\Main\Application::getInstance()->getDocumentRoot();
        
        $logDir = $docRoot . '/upload/syncLogMs';
        
        if (! is_dir($logDir)) {
            
            mkdir($logDir, 0777, true);
        }
        
        file_put_contents($logDir . '/' . $name, ":\n" . var_export($arFields, true) . "\n", FILE_APPEND);
    }

    /**
     * Send rest query to Bitrix24.
     *
     * @param $method -
     *            Rest method, ex: methods
     * @param array $params
     *            - Method params, ex: Array()
     * @param array $auth
     *            - Authorize data, ex: Array('domain' => 'https://test.bitrix24.com', 'access_token' => '7inpwszbuu8vnwr5jmabqa467rqur7u6')
     * @param boolean $authRefresh
     *            - If authorize is expired, refresh token
     * @return mixed
     */
    public static function restCommand($method, array $params = Array(), array $auth = Array(), $authRefresh = true)
    {
        $queryUrl = "https://" . $auth["domain"] . "/rest/" . $method;
        $queryData = http_build_query(array_merge($params, array(
            "auth" => $auth["access_token"]
        )));

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData
        ));

        $result = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($result, 1);

        if ($authRefresh && isset($result['error']) && in_array($result['error'], array(
            'expired_token',
            'invalid_token'
        ))) {
            $auth = restAuth($auth);
            if ($auth) {
                $result = restCommand($method, $params, $auth, false);
            }
        }

        return $result;
    }

    /**
     * Get new authorize data if you authorize is expire.
     *
     * @param array $auth
     *            - Authorize data, ex: Array('domain' => 'https://test.bitrix24.com', 'access_token' => '7inpwszbuu8vnwr5jmabqa467rqur7u6')
     * @return bool|mixed
     */
    public static function restAuth($auth)
    {
        if (! CLIENT_ID || ! CLIENT_SECRET)
            return false;

        if (! isset($auth['refresh_token']) || ! isset($auth['scope']) || ! isset($auth['domain']))
            return false;

        $queryUrl = 'https://' . $auth['domain'] . '/oauth/token/';
        $queryData = http_build_query($queryParams = array(
            'grant_type' => 'refresh_token',
            'client_id' => CLIENT_ID,
            'client_secret' => CLIENT_SECRET,
            'refresh_token' => $auth['refresh_token'],
            'scope' => $auth['scope']
        ));

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl . '?' . $queryData
        ));

        $result = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($result, 1);

        return $result;
    }

    public static function implodeUsedKeys($glue = '', $arParams = array())
    {
        if (! empty($arParams) && strlen($glue) > 0) {

            $strRes = '';

            if (count($arParams) == 1) {

                foreach ($arParams as $key => $val) {

                    $strRes .= $key . '-' . $val;
                }
            } else {

                foreach ($arParams as $key => $val) {

                    $strRes .= $glue . $key . '-' . $val;
                }
            }

            return $strRes;
        } else {
            return false;
        }
    }

    public static function explodeUsedKeys($glue = '', $arParams = array())
    {
        if (! empty($arParams) && strlen($glue) > 0) {

            $arRes = array();

            $arStr = explode($glue, $arParams);
            //
            if (count($arStr) == 1) {

                $arStr = explode('-', $arParams);

                $arRes[$arStr[0]] = $arStr[1];
            } else {

                $arTemp = explode('#', $arParams);

                foreach ($arTemp as $tmpStr) {

                    if (strlen($tmpStr) > 1) {

                        $arStr = explode('-', $tmpStr);
                        $arRes[$arStr[0]] = $arStr[1];
                    }
                }
            }

            return $arRes;
        } else {
            return false;
        }
    }
}
?>